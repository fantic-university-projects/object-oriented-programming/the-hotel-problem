# The hotel problem

- [Description](#description)
  - [How it works](#how-it-works)
- [Math](#math)
  - [Total cost formula](#total-cost-formula)
  - [Ticket cost formula](#ticket-cost-formula)
  - [Hotel accommodation cost formula](#hotel-accommodation-cost-formula)
  - [Transport cost formula](#transport-cost-formula)
  - [Food cost formula](#food-cost-formula)

## Description

My first console application in python, which implements a solution to the problem of calculating the total cost of a trip, in the style of hotel selection applications.

![WorkExample](https://gitlab.com/fantic-university-projects/object-oriented-programming/the-hotel-problem/-/raw/1f03138c54615da738fd2ae0de918db3a2539d38/Resource/WorkExmaple.gif "Work example presentation")

### How it works

- The program welcomes the user and calls itself "AutomaticTravel". 
- The user is prompted to enter one of 5 cities St. Petersburg, Sochi, Beijing, New York and Madrid.  
If the program receives an incorrect input, the program will prompt for the correct value until it receives a match to the condition.
- The user is required to enter information about the number of days of a planned vacation.  
To make the program work correctly and not crash because of the impossibility to turn a string into an int, an algorithm was implemented: if what the user entered does not match the value of the condition, the program displays a text message explaining the error and terminates.  
- Next, the program asks about the class of flight and the number of adult passengers. 
- Then comes one of the most important conditions: whether children are flying. If children are flying, the number of children is asked. If not, 0 will be written to the child_7 and child_14 variables.  
- Next it asks about the hotel, the choice of stars only affects the price of the room, food and extra space for the child.  
- Then "AutomaticTravel" says that in order to save the user money, it can calculate the price of rooms automatically.
  - In case of selecting the number of rooms, the user can set the number of rooms as many as he/she needs, provided that the number of people is less than or equal to the number of beds. 
  - If you choose to calculate automatically, the program will give the number of rooms, so that the number of people and sleeping places coincide. If you have previously selected the option that children are flying, the algorithm works in a similar way, but taking into account the fact that there should be no more than 2 children per 1 adult. In case of violation of this rule, the program will automatically add additional sleeping places and include them in the cost of accommodation. 
- After that there is a request for the number of meals per day, the number of places the user wants to visit (this means that for 1 place, there will be 2 trips, back and forth) and the type of transportation by which he will travel. In the request about the type of transportation not only the price changes, but also the number of cars (in case it is a cab). A regular cab can accommodate up to 4 people inclusive (3 in the back and 1 on the passenger seat). In a VIP-taxi can fit only 3, as the premium cars have a cupholder on the passenger seat in the middle. 
- Before the program gives the total amount of expenses, it makes a delay of 3 seconds, simulating the processing and calculation of the entered data. в 3 секунды, имитируя обработку и подсчет введенных данных.

## Math

The mathematical models (formulas) used by the program to calculate the cost of a trip are described below.

### Total cost formula

The following expression was taken as the basic formula for the calculation:

$$total=bilet+habitation+transport+food$$

where *total* - the total cost of a trip to the city;  
*bilet* - the cost of a ticket to the city;   
*habitation* - the cost of a room in a hotel or a hotel;  
*transport* - the sum of expenses for means of transportation;  
*food* - the sum of expenses for meals in cafes or restaurants.

### Ticket cost formula

Passenger fare is calculated according to the following formula:

$$bilet=(man + child\_7 * 0.5+child\_14 * 0.7) * cost$$

where *man* - the number of adult passengers;  
*child_7* - the number of passengers under 7 years old (the variable is multiplied by 0.5, because we assume that for a child of this age the ticket will cost half of the price for an adult passenger);  
*child_14* - the number of passengers from 7 to 14 years old (the variable is multiplied by 0.7, because we take that for a child of the given age the ticket will cost 70% of the price for an adult passenger);  
*cost* - the cost of a ticket for an adult passenger.

The cost of tickets was taken from 2 sites of airlines: "Aeroflot" and "S7Airlines". We took prices for flights as of June 1, 2020 for Russian cities and August 1, 2020 for foreign cities.

### Hotel accommodation cost formula

The cost of accommodation is calculated according to the following formula:

$$habitation = cost\_day * days + dop\_cost * dsm$$

where *cost_day* is the cost of accommodation per day (on booking sites it is the price per day that is written). The variable is calculated as follows: depending on how many rooms and which hotel was selected, *cost_day* and *dop_cost* will change. If it was selected that children are flying, then *dsm* will be calculated from whether there are more than 2 children per 1 adult. If no, then the required number of seats will be entered in dsm, if yes or if no children are flying, then dsm will be 0. Approximately an extra bed costs 20% of the room price, so *dop_cost* is 20% of a single room of the selected class.

### Transport cost formula

The amount of transport expenses is calculated using the following formula:

$$transport=(t * transport\_cost\_m + child\_7 * transport\_cost\_7 + child\_14 * transport\_cost\_14) * n\_trips * days$$

where *transport_cost_m* - the fare for an adult;  
*transport_cost_7* - the fare for a child under 7 years old, as in many cities, both in Russia and abroad, there is a quota for children of this age;  
*transport_cost_14* - may also vary, but rarely enough;  
*n_trips* - the approximate number of trips that passenger/passengers want to make per day (it is equal to the number of seats multiplied by 2 (round trip)).

There are 3 types of transportation to choose from: public transportation, regular cab and VIP-taxi. Prices for one trip by public transport and cab were taken from different resources.

### Food cost formula

The amount spent on food is calculated using the following formula:

$$food=(man * cost\_eda + child\_7 * 0.7 * cost\_eda + child\_14 * cost\_eda)* n\_eda * days$$

where *cost_eda* - the average check of an adult;  
*n_eda* - the number of meals per day specified by the user. As a rule, there are no discounts in restaurants and cafes, in rare cases there is a children's menu, but it should be taken into account that a child can eat not only it, but also the usual menu, which is mainly provided by restaurants and cafes. But since purely physically a child of such age cannot eat as much as an adult, we take 70% of *cost_eda*.
 
The price for food was calculated in the following way - we searched for the price for breakfast, lunch and dinner in different hotels, summed it up and calculated the arithmetic mean.
